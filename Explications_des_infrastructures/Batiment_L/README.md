# Batiment L

> .12. car 'L' est la 12 lettre de l'alphabet.

| Machine | Port | IP | VLAN |
|---------|------|----|------|
| Router1 | fa1/0.10 | 10.12.10.254 | 10 |
| Router1 | fa1/0.7 | 10.12.7.254 | 7 |
| Router1 | fa1/0.20 | 10.12.20.254 | 20 |
