# Créer l'infrastructure d'une entreprise

## Sommaire

- Matériel Utilisé
- Mise en place:
    - Routeur
    - Switch
    - Serveur ESXI
        - VM Linux 
        - VM Windows Serveur

## Matériel Utilisé

Pour réaliser le projet, nous avons utilisé:

- 1 Routeur Cisco RV320
- 2 Switchs Huawei S2750
- 1 Serveur avec ESXI
- 1 NAS Synologie
- 1 Routeur Opérateur
- 1 Ordinateur/VM Client

## Schéma de l'infrastructure réseau:
Schéma Global:
![Infra Global](./images/infra2.png)

Schéma IP:
![Infra IP](./images/infra3.png)

Table VLAN:
![Table VLAN](./images/table_ip.png)

## Mise en place

### Router RV320

Connection au router avec l'IP `192.168.1.254`:

![Ecran d'accueil](./images/router1.png)

Vérification de la version de logiciel et du certificat:

> **Si le logiciel n'est pas à jour ou que le certificat n'est plus valable, le routeur aura des disfonctionnements**

![version logiciel](./images/routeur2.1.png)
![certificat](./images/routeur2.2.png)

Mise en place du serveur DHCP:

![configuration DHCP](./images/router3.png)

Configuration d'un serveur VPN SSL avec OpenVPN:
![configuration du serveur OpenVPN](./images/router4.1.png)

Configuration des comptes OpenVPN:
![Configuration des comptes OpenVPN](./images/router4.2.png)

Fichier du client OpenVPN:
[Admin.ovpn](./images/Admin.ovpn)

Redirection de port pour le VPN sur le routeur opérateur:
![FAI](./images/vpn.png)

### Switch S2750

Connection aux switchs avec l'IP `192.168.1.253` et `192.168.1.252`:

![ecran accueil](./images/switch1.png)

Vérification du logiciel:
![ ](./images/switch2.png)

Création du VLAN natif:
![ ](./images/switch3.png)

Application sur tous les ports:
![ ](./images/switch4.png)

Création d'un VLAN:
![ ](./images/switch5.png)

Configuration d'un port en mode trunk
![ ](./images/switch6.png)


### Serveur ESXI:

#### VM Linux:

Installation de **InfluxDB**:

```
# apt-get install curl apt-transport-https
# curl -sL https://repos.influxdata.com/influxdb.key | apt-key add -
# echo "deb https://repos.influxdata.com/debian jessie stable" > /etc/apt/sources.list.d/influxdb.list
# apt-get update
# apt-get install influxdb 
```

Installation de **Grafana**:

```
# curl https://packagecloud.io/gpg.key | apt-key add -
# echo "deb https://packagecloud.io/grafana/stable/debian/ jessie main" > /etc/apt/sources.list.d/grafana.list
# apt-get update
# apt-get install grafana
# systemctl daemon-reload
# systemctl enable grafana-Server
# systemctl start grafana-Server 
```
Ajout de la source de données **InfluxDB** dans **Grafana**:
![ ](./images/grafana1.png)
![ ](./images/grafana2.png)

Installation et configuration de **Telegraf**:

```
apt-get install telegraf
```
Dans le fichier `/etc/telegraf/telegraf.conf `

```
[[outputs.influxdb]]
url = "http://IP:8086"
database = "telegraf"
username = "admin"
password = "root"
```

#### VM Window Serveur 

* Serveur Active Dirctory
* GPO 
* Lecteur partagée 

##### **Configuration de l'active directory**

Dans un premier temps il faut configuré l'active directory : 

![Img1](./images/img1.png)

Suivez les étapes, puis une fois sur cette fenêtre sélectionner cela :
![img2](./images/img2.png)

Continuer les étapes jusqu'à arriver à installer.

Une fenetre s'ouvriras en haut à droite cliquer dessus.

![img3](./images/img3.png)

Sur la prochaine fenêtre deffinisez votre nom de domaine :

![img4](./images/img4.png)

Deffinisez ensuite toutes vos informations, mots de passe nom et votre actif directory sera prêt une fois le serveur redemaré.

##### **Utilisateur** 

Pour créer des utilisateurs il faudra chercher la fenêtre suivante : 

![img5](./images/img5.png)

Vous aurez alors plusieurs options celle de créer des utilisateurs, des groupes ou des unités d'organisationsLa création de ses diverses unités dépend de votre organisation et de ce que vous souhaitez mettre en place.

##### **GPO** 

Pour mettre en place des DPO il faut aller chercher dans ceci : 

![img6](./images/img6.png)

Il faudra ensuite choisir sur quel groupe ou unités l'appliquer puis crée la G. Po.

![IMG7](./images/img7.png)

Il faudra sur quoi vous souhaiter qu'agisse votre GPO

![img8](./images/img8.png)

Si vous souhaitez toucher au disque il faudra par exemple aller dans préférence utilisateur > Windows > Agrégation de disque.

Votre Gpo sera alors appliqué sur tout votre groupe ou unité
